import signal
import sys
import traceback
import queue
import busio
from board import SCL, SDA
from adafruit_pca9685 import PCA9685
from adafruit_motor import servo
import time
import numpy as np

from spotmicroai.utilities.log import Logger
from spotmicroai.utilities.config import Config
from spotmicroai.utilities.config import AttributeDict
import spotmicroai.utilities.queues as queues
from spotmicroai.utilities.general import General
from spotmicroai.motion_controller.kinematics import Kinematics

import json 

log = Logger().setup_logger('Motion controller')

class BodyState (AttributeDict):

    def __init__(self):
        super(BodyState, self).__init__()
        self.lf = None
        self.rf = None
        self.lb = None
        self.rb = None
        self.pose = None
        self.center = None

    def update_model(self, kn):
        for name in ['lf', 'rf', 'lb', 'rb']:
            v = getattr(self, name, None)
            if v is not None:
                kn.legs[name].pos = v

        if self.pose is not None:
            kn.pose = self.pose
        if self.center is not None:
            kn.center = self.center

    def __repr__(self):
        d = dict()
        for k in ['lf', 'rf', 'lb', 'rb', 'pose', 'center']:
            v = getattr(self, k, None)
            if v is not None:
                d[k] = v
        return str(d)


def basic_walking_gait(kn):
    kn.update_all_angles([(0,0,0),(0,0,0),(0,0,0)])
    seq = [(-2, 0, 0), (0, 2, 0), (2, 0, 0), (-2, 0, 0)]
    steps = len(seq)
    scale = 1/(steps - 1)

    X, Y, Z = list(zip(*seq))
    phases = [scale * i for i in range(steps)]
    origin = {name : kn.legs[name].pos + [0, 2.5, 0] for name in Kinematics.LEGS}

    def gait(phase):
        state = BodyState()
        for leg, offset in [('lf', 0), ('rf', 0.5), ('lb', 0.5), ('rb', 0)]:
            dx = np.interp(phase + offset, phases, X, period=1)
            dy = np.interp(phase + offset, phases, Y, period=1)
            dz = np.interp(phase + offset, phases, Z, period=1)
            state[leg] = origin[leg] + (dx, dy, dz)
        return state
    return gait


class MotionController:
    def __init__(self, communication_queues):
        try:
            log.debug('Starting controller...')
            self.is_activated = False
            self.speed = 1
            self._gait_phase = 0 
            self._gait = None

            signal.signal(signal.SIGINT, self.exit_gracefully)
            signal.signal(signal.SIGTERM, self.exit_gracefully)

            self.i2c = busio.I2C(SCL, SDA)
            self.load_pca9685_boards_configuration()
            self.load_servos_configuration()

            #self._abort_queue = communication_queues[queues.ABORT_CONTROLLER]
            self._motion_queue = communication_queues[queues.MOTION_CONTROLLER]
            #self._lcd_screen_queue = communication_queues[queues.LCD_SCREEN_CONTROLLER]

            #if self.pca9685_address:
            #    self._lcd_screen_queue.put('motion_controller_1 OK')
            #    self._lcd_screen_queue.put('motion_controller_2 OK')
            #else:
            #    self._lcd_screen_queue.put('motion_controller_1 OK')
            #    self._lcd_screen_queue.put('motion_controller_2 NOK')

            self._previous_event = {}

            # get offsets from config
            lf = [self.servo_config['lf_shoulder'].offset,
                  self.servo_config['lf_leg'].offset,
                  self.servo_config['lf_feet'].offset]
            rf = [self.servo_config['rf_shoulder'].offset,
                  self.servo_config['rf_leg'].offset,
                  self.servo_config['rf_feet'].offset]
            lb = [self.servo_config['lb_shoulder'].offset,
                  self.servo_config['lb_leg'].offset,
                  self.servo_config['lb_feet'].offset]
            rb = [self.servo_config['rb_shoulder'].offset,
                  self.servo_config['rb_leg'].offset,
                  self.servo_config['rb_feet'].offset]
            mirror_lf = [-1,-1,-1]
            mirror_rf = [1,1,1]
            mirror_lb = [-1,-1,-1]
            mirror_rb = [1,1,1]

            self.kn = Kinematics([1.5, 0.666, 4, 5], [7.5, 5.25], [lf, rf, lb, rb],
                    mirror=[mirror_lf, mirror_rf, mirror_lb, mirror_rb])

        except Exception:
            log.error('Error occured when initializating MotionController:\n%s',
                    traceback.format_exc())

    def exit_gracefully(self, signum, frame):
        self.deactivate_pca9685_boards()
        log.info('Terminated')
        sys.exit(0)

    def synchronize_angles(self):
        if not self.is_activated:
            return
        # get current angles from servos
        # and update angles in kinematics model
        lf = (self.servos['lf_shoulder'].angle, 
              self.servos['lf_leg'].angle, 
              self.servos['lf_feet'].angle)
        rf = (self.servos['rf_shoulder'].angle, 
              self.servos['rf_leg'].angle, 
              self.servos['rf_feet'].angle)
        lb = (self.servos['lb_shoulder'].angle, 
              self.servos['lb_leg'].angle, 
              self.servos['lb_feet'].angle)
        rb = (self.servos['rb_shoulder'].angle, 
              self.servos['rb_leg'].angle, 
              self.servos['rb_feet'].angle)
        self.kn.update_all_servo_angles([lf, rf, lb, rb])

    def start_gait(self, gait_f):
        self._gait = gait_f

    def step_gait(self, elapsed_time):
        dp = self.speed * elapsed_time
        next_phase =  (self._gait_phase + dp) % 1
        body_state =  self._gait(next_phase)
        self._gait_phase = next_phase
        print('phase = ', next_phase, end='\r')

        body_state.update_model(self.kn)

    def do_process_events_from_queues(self):
        t0, t, dt = time.time(), 0, 0
        while True:
            try:
                if not self._motion_queue.empty():
                    event = self._motion_queue.get(block=False, timeout=None)
                    print('event: ', {k: v for k, v in event.items() if v})
                else:
                    event = {}

                if event.get('start', None):
                    if self.is_activated:
                        pass
                        #self.deactivate_pca9685_boards()
                        #self._abort_queue.put(queues.ABORT_CONTROLLER_ACTION_ABORT)
                    else:
                        #self._abort_queue.put(queues.ABORT_CONTROLLER_ACTION_ACTIVATE)
                        self.activate_pca9685_boards()
                        self.activate_servos()

                if event.get('a', None):
                    self.start_gait(basic_walking_gait(self.kn.copy()))
                    dt = 0

                if event.get('b', None):
                    self._gait = None

                if self._gait is not None:
                    self.step_gait(dt)

                if event.get('y', None):
                    with open('/home/pi/val.json', 'r') as f:
                        val = json.load(f)
                        for name in ['lf', 'lb', 'rf', 'rb']:
                            if name in val:
                                self.kn.legs[name].angles = np.array(val[name]) * np.pi/180


                if event.get('x', None):
                    self.synchronize_angles()

                t = time.time()
                dt = t - t0
                t0 = t
                #time.sleep(0.01)
                self.move()
            except Exception:
                log.error('Unknown problem while processing the queue of the motion controller')
                log.error(' - Most likely a servo is not able to get to the assigned position:')
                log.error(traceback.format_exc())

    def load_pca9685_boards_configuration(self):
        self.pca_config = []
        try:
            boards = Config().get(Config.MOTION_CONTROLLER_BOARDS_PCA9685)
            for board in boards:
                pca = AttributeDict()
                pca.frequency = board['frequency']
                pca.address = int(board['address'], 16)
                pca.clock = board['reference_clock_speed']

                self.pca_config.append(pca)
        except Exception:
            log.error(traceback.format_exc())

    def activate_pca9685_boards(self):
        self.pca_boards = []

        for config in self.pca_config:
            pca = PCA9685(self.i2c, address=config.address,
                            reference_clock_speed=config.clock)
            pca.frequency = config.frequency
            self.pca_boards.append(pca)
            print(config)

        self.is_activated = True
        log.debug(str(len(self.pca_boards)) + ' PCA9685 board(s) activated')

    def deactivate_pca9685_boards(self):
        deactivated = 0
        try:
            if hasattr(self, 'pca_boards'):
                for pca in self.pca_boards:
                    pca.deinit()
                    deactivated += 1
        finally:
            # self._abort_queue.put(queues.ABORT_CONTROLLER_ACTION_ABORT)
            self.is_activated = False

        log.debug(str(deactivated) + ' PCA9685 board(s) deactivated')

    def load_servos_configuration(self):
        self.servo_config = AttributeDict()
        servos = Config().get(Config.MOTION_CONTROLLER_SERVOS)

        for name, config in servos.items():
            _servo = AttributeDict()
            for k, v in config.items():
                _servo[k] = v
            self.servo_config[name] = _servo

    def activate_servos(self):
        self.servos = AttributeDict()
        for name, s in self.servo_config.items():
            pca = self.pca_boards[s.board]
            _servo = servo.Servo(pca.channels[s.channel], actuation_range=270)
            _servo.set_pulse_width_range(min_pulse=s.min_pulse, max_pulse=s.max_pulse)

            self.servos[name] = _servo

    def move(self):
        if not self.is_activated:
            return

        lf = self.kn.legs['lf'].servo_angles
        rf = self.kn.legs['rf'].servo_angles
        lb = self.kn.legs['lb'].servo_angles
        rb = self.kn.legs['rb'].servo_angles
        #print('angles: ', { name : list(self.kn.legs[name].angles*180/np.pi) for name in ['lf', 'rf', 'lb', 'rb']})

        legnames = ['lf', 'rf', 'lb', 'rb']
        exts = ['shoulder', 'leg', 'feet']

        for legname in legnames:
            angles = self.kn.legs[legname].servo_angles

            for i, ext in enumerate(exts):
                current = legname + '_' + ext
                try:
                    self.servos[current].angle = angles[i]
                except ValueError:
                    log.error('Impossible {} angle {} requested'.format(current, angles[i]))


