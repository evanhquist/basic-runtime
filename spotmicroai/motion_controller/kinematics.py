import numpy as np
from  math import atan2, cos, sin, acos, pi, sqrt

class Leg:
    def __init__(self, legname, kmodel, offsets=[0,0,0], mirror=[1,1,1]):
        if kmodel is None:
            raise ValueError('kmodel cannot be None')

        self._offsets = np.array(offsets)
        self._angles = np.array([0,0,0])
        self._mirror = np.array(mirror)
        self._legname = legname
        self._kmodel =  kmodel
        Wp = kmodel.matrices[legname] @ kmodel.calc_coordinates(*self._angles)[-1]
        self._pos = Wp[:-1]

    @property
    def name(self):
        return self._legname

    @property
    def pos(self):
        return np.array(self._pos)

    @pos.setter
    def pos(self, new_pos):
        new_pos = np.array(new_pos)
        valid = self._kmodel._do_leg_pos_change(self._legname, new_pos)
        if valid:
            self._pos = new_pos

    @property
    def angles(self):
        return np.array(self._angles)

    @angles.setter
    def angles(self, new_angles):
        self._angles = np.array(new_angles)
        self._kmodel._do_angle_change(self._legname)

    @property
    def servo_angles(self):
        return self._angles * self._mirror * 180/ np.pi + self._offsets

    @servo_angles.setter
    def servo_angles(self, servo_angles):
        self.angle = (servo_angles - self._offsets) * np.pi/180 * self._mirror



class Kinematics:

    LEGS = ['lf', 'rf', 'lb', 'rb']
    """
    List strings used as keys in `Kinematics.legs` to stores the legs.
    The order of the legs is the order in which `Kinematics.calc_forward_matrix`
    returns the a tuple of matrices for the legs.
    """


    def __init__(self, lengths, bodysize, angle_offsets=[(0,0,0), (0,0,0), (0,0,0), (0,0,0)],
            mirror=[(1,1,1),(1,1,1),(1,1,1),(1,1,1)]):
        """
        @param lengths      a list of the legs lengths from the shoulder to 
                            the foot [l1, l2, l3, l4]
        @param bodysize     a list of the body length L and width W, [L, W]
        @param angle_offsets    a list of angle offsets for each of the legs specified
                            in the order [lf, rf, lb, rb] (same as `Kinematics.LEGS`). 
                            Each angle offset is a list of numbers specifying the offset
                            for each of the joint angles (theta1, theta2, theta3) starting
                            from the shoulder.

                            The offsets are the servo angles needed to make the shoulder
                            90 degrees with the body and the arms face straight down.
        @param mirror       a list of either 1 or -1 in the same format as `angle_offsets`
                            indicating whether the positive direction for positive angles
                            is flipped
        """
        self.lengths = tuple(lengths)
        self.bodysize = tuple(bodysize)

        self._bodypose = np.array([0, 0, 0])
        self._center = np.array([0, 0, 0])
        self.matrices = {}

        self._update_matrices()
        self._legs = {Kinematics.LEGS[i]: Leg(Kinematics.LEGS[i], self, offsets=angle_offsets[i],
            mirror=mirror[i])
                for i in range(len(Kinematics.LEGS))}


    def copy(self):
        """
        Return a copy of the model.
        """
        angle_offsets = [ self.legs[name]._offsets for name in Kinematics.LEGS ]
        mirror        = [ self.legs[name]._mirror  for name in Kinematics.LEGS ]

        return Kinematics(self.lengths, self.bodysize, angle_offsets, mirror)


    @property
    def center(self):
        return self._center


    @center.setter
    def center(self,  new_center):
        new_center = np.array(new_center)

        leg_pos = [self.legs[name]._pos for name in Kinematics.LEGS]
        valid = self._do_pos_change(self._bodypose, new_center, leg_pos)
        if valid:
            self._center = new_center


    @property
    def bodypose(self):
        return self._bodypose


    @bodypose.setter
    def bodypose(self, new_pose):
        new_pose = np.array(new_pose)

        leg_pos = [self.legs[name]._pos for name in Kinematics.LEGS]
        valid = self._do_pos_change(new_pose, self._center, leg_pos)
        if valid:
            self._bodypose = new_pose


    @property
    def legs(self):
        return self._legs


    def update_all_servo_angles(self, all_angles):
        for legname, angles in zip(Kinematics.LEGS, all_angles):
            self.legs[legname].servo_angles = angles

    def update_all_angles(self, all_angles):
        for legname, angles in zip(Kinematics.LEGS, all_angles):
            self.legs[legname]._angles = np.array(angles)
            self._do_angle_change(legname)


    def get_all_legpoints(self):
        """
        Returns a list containing the world-space coordinates of all
        joints of all four legs in the order listed in `Kinematics.LEGS`
        """
        points = []
        for leg in Kinematics.LEGS:
            Lp = self.calc_coordinates(*self.legs[leg]._angles)
            T = self.matrices[leg]
            Wp = [T@p for p in Lp]
            points.append(Wp)
        return points


    def get_all_bodypoints(self):
        """
        Returns the four body points in world-space coordinates
        in the order listed in `Kinematics.LEGS`
        """
        points = []
        for leg in ['lf', 'lb', 'rf', 'rb']:
            T = self.matrices[leg]
            points.append(T@[0,0,0,1])
        return points


    def _update_matrices(self, matrices = None):
        """
        Recomputes the transformation matrices for changing between
        leg-space to world-space and updates `Kinematics.matrices`
        using the current bodypose (`Kinematics.bodypose`) and 
        center (`Kinematics.center`)
        If `matrices` is provided, then uses that instead of recomputing.
        The order should match the order of the legs listed in `Kinematics.LEGS`
        """
        #Tlf,Trf,Tlb,Trb,
        if matrices is None:
            matrices = self.calc_forward_matrix(*self._bodypose, *self._center)
        for i, leg in enumerate(Kinematics.LEGS):
            self.matrices[leg] = matrices[i]
        return matrices


    def _do_leg_pos_change(self, legname, leg_pos):
        """
        Attempts to updates the joint angles for leg with key `legname`
        given it's current position `Leg.pos` in leg-space.
        If impossible, returns False otherwise rturns True.
        """
        leg = self.legs[legname]
        Wp = Kinematics.__get4dcopy(leg_pos)
        T = self.matrices[legname]
        Lp = np.linalg.inv(T) @ Wp

        angles = self.calc_angles(Lp[0], Lp[1], Lp[2])
        if angles is None:
            return False

        leg._angles = np.array(angles)
        return True


    def _do_pos_change(self, bodypose, center, leg_pos):
        """
        Attempts to update the necessary joint angles using IK model
        given the leg positions (from `Kinematics.legs`), body pose
        (from `Kinematics.bodypose`), and center (from `Kinematics.center`).
        Returns `True` if succesful, otherwise `False`.
        """
        #(Tlf,Trf,Tlb,Trb,Tm)
        res = self._validate_state(bodypose, center, leg_pos)
        if res is False:
            return False

        matrices, all_angles = res
        self._update_matrices(matrices)
        for leg, angles in zip(Kinematics.LEGS, all_angles):
            self.legs[leg]._angles = np.array(angles)
        return True


    def _do_angle_change(self, legname):
        """
        Updates the position of the leg with key `keyname` 
        in leg-space using forward kinematic model given the current joint angles.
        """
        leg = self.legs[legname]
        theta1, theta2, theta3 = leg._angles

        Lp = self.calc_coordinates(theta1, theta2, theta3)
        Wp = self.matrices[legname]@Lp[-1]
        leg._pos = Wp[:-1]


    def calc_angles(self, x, y, z):
        """
        Returns the joint angles (theta1, theta2, theta2)
        needed for foot to reach (x,y,z) in leg-space given
        the lengths specified in `Kinematics.lengths`
        """
        l1,l2,l3,l4 = self.lengths

        # length of shoulder-point to target-point on x/y only
        r = x**2+y**2-l1**2
        if r < 0:
            return None

        F=sqrt(r)
        # length we need to reach to the point on x/y
        G=F-l2  
        # 3-Dimensional length we need to reach
        H=sqrt(G**2+z**2)

        theta1=-atan2(y,x)-atan2(F,-l1)
        r = (H**2 + l3**2 - l4**2)/(2*l3*H)
        if r < -1 or r > 1:
            return None
        theta2 = atan2(z, G) - acos(r) 

        r = (H**2 - l3**2 - l4**2)/(2*l3*l4)
        if r < -1 or r > 1:
            return None
        theta3 = acos(r)

        return (theta1,theta2,theta3)


    def calc_coordinates(self, theta1, theta2, theta3):
        """
        Returns the (x,y,z) position of all the joints in leg-space
        given the joint angles (theta1, theta2, theta3) and lengths
        in `Kinematics.lengths`
        """
        theta23=theta2+theta3

        l1,l2,l3,l4 = self.lengths
        T0=np.array([0,0,0,1])
        T1=T0+np.array([-l1*cos(theta1),l1*sin(theta1),0,0])
        T2=T1+np.array([-l2*sin(theta1),-l2*cos(theta1),0,0])
        T3=T2+np.array([-l3*sin(theta1)*cos(theta2),-l3*cos(theta1)*cos(theta2),l3*sin(theta2),0])
        T4=T3+np.array([-l4*sin(theta1)*cos(theta23),-l4*cos(theta1)*cos(theta23),l4*sin(theta23),0])

        return np.array([T0,T1,T2,T3,T4])


    def  _validate_state(self, bodypose, center, leg_pos):
        """
        Returns False if given configuration of bodypose, center, and leg position
        is impossible. Otherwise returns a tuple of transformation matrices and
        joint angles.
        The list of leg positions in world-space, `leg_pos`, must be specified 
        in this order listed in `Kinematics.LEGS`.
        The transformation matrices and angles are returned in the same order.
        """
        matrices = self.calc_forward_matrix(*bodypose, *center)
        Lp = [np.linalg.inv(matrices[i]) @ Kinematics.__get4dcopy(leg_pos[i]) for i in range(len(leg_pos))]
        all_angles = []
        for p in  Lp:
            angles = self.calc_angles(p[0], p[1], p[2])
            if angles is None:
                return False
            all_angles.append(angles)
        return (matrices, all_angles)


    def calc_forward_matrix(self, omega, phi, psi, xm, ym, zm):
        """
        Calculate the transformation matrices for going from leg-space to world-space
        given the body size (length L, and width W) specified in `Kinematics.bodysize`.
        The "front" towards the +x direction.
        The "right" is towards the +z direction.

        Returns the tuple (Tlf,Trf,Tlb,Trb,Tm) where
            Tlf, Trf, Tlb, Trb: matrices for left-front(lf) leg, etc...
            Tm: translation matrix
        The order of legs in `Kinematics.legs` should match order in which
        the matrices are returned.
        """
        L, W = self.bodysize

        Rx = np.array([
            [1, 0, 0, 0], 
            [0, np.cos(omega), -np.sin(omega), 0],
            [0,np.sin(omega),np.cos(omega),0],
            [0,0,0,1]])

        Ry = np.array([
            [np.cos(phi),0, np.sin(phi), 0], 
            [0, 1, 0, 0],
            [-np.sin(phi),0, np.cos(phi),0],
            [0,0,0,1]])

        Rz = np.array([
            [np.cos(psi),-np.sin(psi), 0,0], 
            [np.sin(psi),np.cos(psi),0,0],
            [0,0,1,0],
            [0,0,0,1]])

        Rxyz=Rx@Ry@Rz

        T = np.array([[0,0,0,xm],[0,0,0,ym],[0,0,0,zm],[0,0,0,0]])
        Ix=np.array([[-1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])

        Tm = T+Rxyz

        Trb = Tm @ Ix @ np.array([
            [np.cos(-pi/2),0,np.sin(-pi/2),-L/2],
            [0,1,0,0],
            [-np.sin(-pi/2),0,np.cos(-pi/2),-W/2],
            [0,0,0,1]])

        Trf = Tm @ Ix @ np.array([
            [np.cos(-pi/2),0,np.sin(-pi/2),L/2],
            [0,1,0,0],
            [-np.sin(-pi/2),0,np.cos(-pi/2),-W/2],
            [0,0,0,1]])

        Tlf = Tm @ np.array([
            [np.cos(pi/2),0,np.sin(pi/2),L/2],
            [0,1,0,0],
            [-np.sin(pi/2),0,np.cos(pi/2),W/2],
            [0,0,0,1]])

        Tlb = Tm @ np.array([
            [np.cos(pi/2),0,np.sin(pi/2),-L/2],
            [0,1,0,0],
            [-np.sin(pi/2),0,np.cos(pi/2),W/2],
            [0,0,0,1]])

        return (Tlf,Trf,Tlb,Trb,Tm)

    def __get4dcopy(arr3d):
        return np.array([arr3d[0], arr3d[1], arr3d[2], 1])
