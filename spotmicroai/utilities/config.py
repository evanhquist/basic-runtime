import json
import sys
import os
from spotmicroai.utilities.log import Logger
import jmespath  # http://jmespath.org/tutorial.html
import shutil
import traceback
from pathlib import Path
from collections import defaultdict

log = Logger().setup_logger('Configuration')


class AttributeDict(defaultdict):
    def __init__(self):
        super(AttributeDict, self).__init__(AttributeDict)

    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            raise AttributeError(key)

    def __setattr__(self, key, value):
        self[key] = value

class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Config(metaclass=Singleton):
    ABORT_CONTROLLER_GPIO_PORT = 'abort_controller[0].gpio_port'
    LCD_SCREEN_CONTROLLER_I2C_ADDRESS = 'lcd_screen_controller[0].lcd_screen[0].address'
    REMOTE_CONTROLLER_CONTROLLER_DEVICE = 'remote_controller_controller[0].remote_controller[0].device'

    MOTION_CONTROLLER_BOARDS_PCA9685 = 'motion_controller[*].boards.pca9685s | [0]'
    MOTION_CONTROLLER_SERVOS = 'motion_controller[*].servos | [0]'

    values = {}

    def __init__(self):

        try:
            log.debug('Loading configuration...')

            self.load_config()
            self.list_modules()

        except Exception as e:
            log.error('Problem while loading the configuration file:\n%s',
                    traceback.format_tb(e.__traceback__))

    def load_config(self):
        try:
            if not os.path.exists(str(Path.home()) + '/spotmicroai.json'):
                shutil.copyfile(str(Path.home()) + '/spotmicroai/spotmicroai.default',
                                str(Path.home()) + '/spotmicroai.json')

            with open(str(Path.home()) + '/spotmicroai.json') as json_file:
                self.values = json.load(json_file)
                # log.debug(json.dumps(self.values, indent=4, sort_keys=True))

        except Exception as e:
            log.error("Configuration file don't exist or is not a valid json, aborting.")
            sys.exit(1)

    def list_modules(self):
        log.info('Detected configuration for the modules: ' + ', '.join(self.values.keys()))

    def save_config(self, path = None):
        if path is None:
            path = str(Path.home()) + '/spotmicroai.json'
        try:
            if os.path.exists(path):
                backup = path + '0'
                while os.path.exists(backup):
                    backup += '0'
                log.info('Copy {} to {}...'.format(path, backup))
                shutil.copyfile(path, backup)

            with open(path, 'w') as outfile:
                json.dump(self.values, outfile, indent='  ')
            log.info('Saved config to {}'.format(path))
        except Exception as e:
            log.error("Problem saving the configuration file:\n%s",
                    traceback.format_tb(e.__traceback__))

    def get(self, search_pattern):
        log.debug(search_pattern + ': ' + str(jmespath.search(search_pattern, self.values)))

        return jmespath.search(search_pattern, self.values)
