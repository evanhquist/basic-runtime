#!/home/pi/spotmicroai/venv/bin/python3 -u
import traceback

import busio
from board import SCL, SDA
from adafruit_pca9685 import PCA9685
from adafruit_motor import servo
from pick import pick
import time
import os
import sys
from spotmicroai.utilities.config import Config
import RPi.GPIO as GPIO

from spotmicroai.utilities.log import Logger
from spotmicroai.utilities.config import Config

log = Logger().setup_logger('CALIBRATE SERVOS')

log.info('Calibrate servo angles...')

pca = None

gpio_port = Config().get(Config.ABORT_CONTROLLER_GPIO_PORT)

GPIO.setmode(GPIO.BCM)
GPIO.setup(gpio_port, GPIO.OUT)
GPIO.output(gpio_port, False)

i2c = busio.I2C(SCL, SDA)
LEG_KEYS = [
        'lb_shoulder', 'lb_leg', 'lb_feet',
        'rb_shoulder', 'rb_leg', 'rb_feet',
        'rf_shoulder', 'rf_leg', 'rf_feet',
        'lf_shoulder', 'lf_leg', 'lf_feet',
    ]
LEGS = Config().get(Config.MOTION_CONTROLLER_SERVOS)
BOARDS = Config().get(Config.MOTION_CONTROLLER_BOARDS_PCA9685)

angle_prop = 'rest_angle'
selected_index = -1

while True:
    options = ['{:20s} - CHANNEL[{:2d}] - {}[{:>3s}]'
            .format(name, leg['channel'], angle_prop, str(leg.get(angle_prop, '?')))
            for name, leg in LEGS.items()]

    title = 'The folder integration_tests for more tests' + os.linesep + \
            '1. Use "i2cdetect -y 1" to identify your i2c address' + os.linesep + \
            '2. Write your pca9685 i2c address(es) and settings in your configuration file ~/spotmicroai.json' + os.linesep + \
            '3. if no angle is specified 90 will be the default position, for example if you just press Enter' + os.linesep + \
            '' + os.linesep + \
            'Write "menu" or "m" and press Enter to return to the list of servos' + os.linesep + \
            'Write "change <prop>" or "c <prop>" and press Enter to change the property being calibrated ' + os.linesep + \
            'Write "write <path>" or "w <path>" and press Enter write changes to config file' + os.linesep + \
            'Write "exit" or "e" and press Enter to exit' + os.linesep + \
            '' + os.linesep + \
            'Please choose the servo to calibrate \'{}\': '.format(angle_prop)

    selected_option, selected_index = pick(options, title, default_index=(selected_index+1) % len(options))
    sel_legname = selected_option.split()[0]
    selected_leg = LEGS[sel_legname]
    board = BOARDS[selected_leg['board']]

    print('Current: ', selected_leg)
    print('Board: ', board)

    while True:
        try:
            user_input = input("Write the value for {} and press Enter: "
                    .format(angle_prop))
            args = user_input.split()
            command = args[0] if len(args) > 0 else ''

            pca = PCA9685(i2c, address=int(board['address'], 16), reference_clock_speed=board['reference_clock_speed'])
            pca.frequency = board['frequency']
            active_servo = servo.Servo(pca.channels[selected_leg['channel']], actuation_range=270)
            active_servo.set_pulse_width_range(min_pulse=selected_leg['min_pulse'], max_pulse=selected_leg['max_pulse'])

            if command == 'menu' or command == 'm' or command == '':
                break
            elif command == 'change' or command == 'c':
                if len(args) >= 2:
                    angle_prop = args[1]
            elif command == 'write' or command == 'w':
                if len(args) >= 2:
                    Config().save_config(args[1])
                else:
                    Config().save_config()
            elif command == 'exit' or command == 'e':
                sys.exit(0)
            else:
                active_servo.angle = int(user_input)
                selected_leg[angle_prop] = int(user_input)
            time.sleep(0.1)
        except Exception as e:
            traceback.print_exc()
        finally:
            val = None
            pca.deinit()
